package com.rancedxk.monitor.utils;

import com.sun.jna.Native;
import com.sun.jna.Platform;

public class LibraryUtil {
	
	/**
	 * 通过Native加载库文件，并实例化<br/>
	 * 注意：库文件需要放在项目src/main/resources/library目录下，并根据dll对应的os类型放在相应目录下<br/>
	 * 如：<br/>
	 * 库文件：NetServerInterface.dll<br/>
	 * 存放路径：src/main/resources/library/win32/broadcast/NetServerInterface.dll
	 * 引用方式：
	 * @param library 库文件路径
	 * @return
	 */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T>T loadLibrary(String libraryName, Class clazz) {
        return (T)Native.loadLibrary(getLibraryPath(libraryName), clazz);
    }

    /**
	 * 通过Native加载库文件，并实例化<br/>
	 * 注意：库文件需要放在项目src/main/resources/library目录下，并根据dll对应的os类型放在相应目录下<br/>
	 * 如：<br/>
	 * 库文件：NetServerInterface.dll<br/>
	 * 存放路径：src/main/resources/library/win32/broadcast/NetServerInterface.dll
	 * 引用方式：
     * @param path 库文件所在目录
     * @param libraryName 库文件名
	 * @return
	 */
    @SuppressWarnings({ "rawtypes" })
    public static <T>T loadLibrary(String path, String libraryName, Class clazz) {
        return loadLibrary(path + "\\" + libraryName, clazz);
    }
    
    /**
     * 获取指定库文件的相对路径，用于外部初始化
     * @param libraryName 库文件相对路径（所在目录+库文件名）
     */
    public static String getLibraryPath(String libraryName) {
    	String contextPath = (LibraryUtil.class.getResource("/").getPath()).replaceAll("%20", " ").substring(1).replace("/", "\\") ;
        String osPrefix = getOsPrefix();
        if(osPrefix.toLowerCase().startsWith("win32-x86")) {
        	contextPath += "library\\win32\\";
        } else if(osPrefix.toLowerCase().startsWith("win32-amd64") ) {
        	contextPath += "library\\win64\\";
        } else if(osPrefix.toLowerCase().startsWith("linux-i386")) {
        	contextPath = "";
        }else if(osPrefix.toLowerCase().startsWith("linux-amd64")) {
        	contextPath = "";
        }
        return contextPath + libraryName;
    }

    /**
     * 获取指定库文件的相对路径，用于外部初始化
     * @param path 库文件所在目录
     * @param libraryName 库文件名
     */
    public static String getLibraryPath(String path, String libraryName) {
    	return getLibraryPath(path + "\\" + libraryName);
    }

    // 获取操作平台信息
    private static String getOsPrefix() {
        String arch = System.getProperty("os.arch").toLowerCase();
        final String name = System.getProperty("os.name");
        String osPrefix;
        switch(Platform.getOSType()) {
            case Platform.WINDOWS: {
                if ("i386".equals(arch))
                    arch = "x86";
                osPrefix = "win32-" + arch;
            }
            break;
            case Platform.LINUX: {
                if ("x86".equals(arch)) {
                    arch = "i386";
                }
                else if ("x86_64".equals(arch)) {
                    arch = "amd64";
                }
                osPrefix = "linux-" + arch;
            }
            break;
            default: {
                osPrefix = name.toLowerCase();
                if ("x86".equals(arch)) {
                    arch = "i386";
                }
                if ("x86_64".equals(arch)) {
                    arch = "amd64";
                }
                int space = osPrefix.indexOf(" ");
                if (space != -1) {
                    osPrefix = osPrefix.substring(0, space);
                }
                osPrefix += "-" + arch;
            }
            break;

        }

        return osPrefix;
    }
}
