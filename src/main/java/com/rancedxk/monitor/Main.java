package com.rancedxk.monitor;

import java.io.IOException;

import com.rancedxk.monitor.api.APIManager;
import com.rancedxk.monitor.api.Brand;
import com.rancedxk.monitor.api.IApi;
import com.rancedxk.monitor.utils.Kv;

public class Main {
	
	static void control(Brand brand,String code,String ip,String username,String password,int port,String command,Kv params){
		IApi api = APIManager.getApiInstance(brand);
		int loginUserHandle = api.login(code, ip, username, password, port);
		api.control(code, loginUserHandle, command, params);
	}
	
	public static void main(String[] args) throws IOException, NumberFormatException, InterruptedException {

		//向右移动命令
		String command = IApi.COMMANDS.get("PAN_RIGHT");
		//移动参数：速度为4，通道为1
		Kv params = Kv.create().set("speed", 4).set("channel", 1);
		
		//测试海康威视监控
		control(Brand.Hikvision, "192_168_1_34", "192.168.1.34", "admin", "123456", 8000, command, params);
		//测试宇视监控
		control(Brand.UniView, "192_168_0_19", "192.168.0.19", "admin", "123456", 81, command, params);
	}
}
