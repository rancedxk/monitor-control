package com.rancedxk.monitor.lib;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.win32.StdCallLibrary;

import com.rancedxk.monitor.utils.LibraryUtil;


public interface UniViewNetSDK extends StdCallLibrary {
    UniViewNetSDK INSTANCE = LibraryUtil.loadLibrary("monitor\\uniview","NetDEVSDK.dll", UniViewNetSDK.class);

    int NETDEV_GetLastError();
    boolean  NETDEV_Init();
    boolean  NETDEV_SetExceptionCallBack(FExceptionCallBack cbExceptionCallBack, Pointer lpUserData);
    int  NETDEV_Login(String pszDevIP, short dwDevPort, String pszUserName, String pszPassword, NETDEV_DEVICE_INFO_S pstDevInfo);
    boolean NETDEV_PTZControl_Other(int lpUserID, int dwChannelID, int dwPTZCommand, int dwSpeed);
    boolean  NETDEV_Logout(int lpUserID);
    boolean  NETDEV_Cleanup();

    public static interface FExceptionCallBack extends StdCallCallback {
        public void invoke(int lpUserID, int dwType, Pointer lpExpHandle, Pointer lpUserData);
    }

    //NETDEV_Login参数
    public static class NETDEV_DEVICE_INFO_S extends Structure {
        public int dwDevType;
        public short dwAlarmInPortNum;
        public short dwAlarmOutPortNum;
        public int dwChannelNum;
        public byte[] byRes = new byte[48];
        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList("dwDevType","dwAlarmInPortNum","dwAlarmOutPortNum","dwChannelNum","byRes");
        }
    }

    //sdk网络环境枚举变量，用于远程升级
    public static interface NETDEV_LOGIN_PROTO_E {
        public static final int NETDEV_LOGIN_PROTO_ONVIF = 0;
        public static final int NETDEV_LOGIN_PROTO_PRIVATE = 1;
    }
    //设备类型枚举
    public static interface NETDEV_DEVICE_TYPE_E {
        public static final int NETDEV_DTYPE_UNKNOWN = 0;/* 未知*/
        public static final int NETDEV_DTYPE_IPC = 1;/* IPC */
        public static final int NETDEV_DTYPE_IPC_FISHEYE = 2;/* 非经济型鱼眼*/
        public static final int NETDEV_DTYPE_IPC_ECONOMIC_FISHEYE = 3;/* 经济型鱼眼*/
        public static final int NETDEV_DTYPE_NVR = 101;/* NVR */
        public static final int NETDEV_DTYPE_NVR_BACKUP = 102;/* NVR备份服务器*/
        public static final int NETDEV_DTYPE_HNVR = 103;/* 混合NVR */
        public static final int NETDEV_DTYPE_DC = 201;/* DC */
        public static final int NETDEV_DTYPE_DC_ADU = 202;/* ADU */
        public static final int NETDEV_DTYPE_EC = 301;/* EC */
        public static final int NETDEV_DTYPE_VMS = 501;/* VMS */
        public static final int NETDEV_DTYPE_FG = 601;/* FG */
        public static final int NETDEV_DTYPE_INVALID = 0xffffff;/* 无效值*/
    }
    //设备云台控制枚举变量
    public static interface NETDEV_PTZ_E {
        int NETDEV_PTZ_ZOOMTELE_STOP = 0x0301;			/* 放大停止  Zoom in stop */
        int NETDEV_PTZ_ZOOMTELE = 0x0302;				/* 放大  Zoom in */
        int NETDEV_PTZ_ZOOMWIDE_STOP = 0x0303;			/* 缩小停止  Zoom out stop */
        int NETDEV_PTZ_ZOOMWIDE = 0x0304;				/* 缩小  Zoom out */
        int NETDEV_PTZ_TILTUP = 0x0402;					/* 向上  Tilt up */
        int NETDEV_PTZ_TILTDOWN = 0x0404;				/* 向下  Tilt down */
        int NETDEV_PTZ_PANRIGHT = 0x0502;				/* 向右  Pan right */
        int NETDEV_PTZ_PANLEFT = 0x0504;				/* 向左  Pan left */
        int NETDEV_PTZ_LEFTUP = 0x0702;					/* 左上  Move up left */
        int NETDEV_PTZ_LEFTDOWN = 0x0704;				/* 左下  Move down left */
        int NETDEV_PTZ_RIGHTUP = 0x0802; 				/* 右上  Move up right */
        int NETDEV_PTZ_RIGHTDOWN = 0x0804;				/* 右下  Move down right */
        int NETDEV_PTZ_ALLSTOP = 0x0901;				/* 全停命令字  All-stop command word */
        int NETDEV_PTZ_FOCUS_AND_IRIS_STOP = 0x0907;	/* 聚焦.光圈停止  Focus & Iris-stop command word */
        int NETDEV_PTZ_MOVE_STOP = 0x0908;				/* 移动停止命令字  move stop command word */
        int ETDEV_PTZ_ZOOM_STOP = 0x0909;				/* 变倍停止命令字  zoom stop command word */
    }
}
