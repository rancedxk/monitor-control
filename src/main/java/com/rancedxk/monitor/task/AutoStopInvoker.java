package com.rancedxk.monitor.task;

public interface AutoStopInvoker {
	public void onStart();
	public void onStop();
}
