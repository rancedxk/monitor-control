package com.rancedxk.monitor.task;

public class AutoStopTask implements Runnable{
	private boolean isRunning = false;
	//记录线程开始时间，单位毫秒
	private long startTime;
	//持续时长，单位毫秒
	private long duration;
	//运行间隔，用来设置线程睡眠时间，单位毫秒
	private long gap = 500;
	//key
	private String key;
	//执行器
	private AutoStopInvoker invoker;
	
	public AutoStopTask(String key,AutoStopInvoker invoker,long duration) {
		this.key = key;
		this.invoker = invoker;
		this.duration = duration;
	}
	
	public String getKey() {
		return key;
	}
	
	public void setGap(long gap) {
		this.gap = gap;
	}
	
	public boolean isRunning() {
		return isRunning;
	}
	
	/**
	 * 设置开始时间为当前时间，以此来延长转换持续时间
	 */
	public void spirit() {
		if(!this.isRunning){
			//如果当前已经停止，则重新运行
			this.run();
		}else{
			//如果正在运行，则重新设置startTime
			this.startTime = System.currentTimeMillis();
		}
	}
	
	@Override
	public void run() {
		//开始
		isRunning = true;
		//记录开始时间
		startTime = System.currentTimeMillis();
		//运行，设置视频源和推流地址
		invoker.onStart();
		//无限运行，直到运行时间达到最大持续时长
		while(true){
			if(System.currentTimeMillis() - startTime > duration){
				//执行停止方法，并退出循环
				invoker.onStop();
				break;
			}
			//线程睡眠时间，防止无尽循环
			try {
				Thread.sleep(gap);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
    	//运行结束，从池中移除
	    AutoStopTaskManager.remove(getKey());
	    //结束
	    isRunning = false;
	}
}
