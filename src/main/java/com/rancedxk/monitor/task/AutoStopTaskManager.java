package com.rancedxk.monitor.task;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AutoStopTaskManager {
	/**线程数*/
    private static int threadNum = Runtime.getRuntime().availableProcessors() < 4 ? 3 : Runtime.getRuntime().availableProcessors();
    /**共享给Worker的线程数*/
    private static int shareToWorkerThreadNum = threadNum > 4 ? threadNum >> 2 : threadNum - 2;
    /**Worker线程数*/
    private static int workerThreadNum = threadNum - shareToWorkerThreadNum;
	
	private static ExecutorService taskExecutor = Executors.newFixedThreadPool(workerThreadNum);

	static Map<String,AutoStopTask> tasks = new ConcurrentHashMap<String,AutoStopTask>();
	
	public static AutoStopTask create(String key, AutoStopInvoker invoker, long duration){
		AutoStopTask task = new AutoStopTask(key, invoker, duration);
		taskExecutor.execute(task);
		tasks.put(key, task);
		return task;
	}
	
	public static AutoStopTask get(String code){
		return tasks.get(code);
	}
	
	public static boolean isContain(String code){
		return tasks.containsKey(code);
	}
	
	public static AutoStopTask remove(String code){
		AutoStopTask task = null;
		if(tasks.containsKey(code)){
			task = tasks.remove(code);
		}
		return task;
	}

	public static Map<String, AutoStopTask> getTasks() {
		return tasks;
	}
}
