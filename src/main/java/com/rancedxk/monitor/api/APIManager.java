package com.rancedxk.monitor.api;

import java.util.HashMap;
import java.util.Map;

import com.rancedxk.monitor.api.impl.HikvisionApi;
import com.rancedxk.monitor.api.impl.UniViewApi;

public class APIManager {
	
	private static Map<Brand,IApi> instances = new HashMap<Brand,IApi>(){
		private static final long serialVersionUID = 1L;

		{
			put(Brand.Hikvision, new HikvisionApi());
			put(Brand.UniView, new UniViewApi());
		}
	};
	
	public static IApi getApiInstance(Brand brand){
		return instances.get(brand);
	}
}
