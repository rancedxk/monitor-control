package com.rancedxk.monitor.api;

import java.util.HashMap;
import java.util.Map;

import com.rancedxk.monitor.utils.Kv;

public interface IApi {
	
	/**
	 * 通用监控设备控制命令
	 */
	public static Map<String,String> COMMANDS = new HashMap<String,String>(){
		private static final long serialVersionUID = 1L;

		{
			put("FOCUS_NEAR","FOCUS_NEAR"); //焦距放大
			put("FOCUS_FAR","FOCUS_FAR"); //焦距缩小
			put("IRIS_OPEN","IRIS_OPEN"); //光圈扩大
			put("IRIS_CLOSE","IRIS_CLOSE"); //光圈缩小
			put("TILT_UP","TILT_UP"); // 云台上仰
			put("TILT_DOWN","TILT_DOWN"); // 云台下俯
			put("PAN_LEFT","PAN_LEFT"); // 云台左转
			put("PAN_RIGHT","PAN_RIGHT"); // 云台右转
			put("UP_LEFT","UP_LEFT"); // 云台左转上仰
			put("UP_RIGHT","UP_RIGHT"); // 云台右转上仰
			put("DOWN_LEFT","DOWN_LEFT"); // 云台左转下俯
			put("DOWN_RIGHT","DOWN_RIGHT"); // 云台右转下俯
		}
	};
	
	/**
	 * 设备操作
	 * @param code 设备编码
	 * @param ip IP地址
	 * @param username 帐号
	 * @param password 密码
	 * @param port 服务端口
	 * @return 登录会话标识/句柄，-1表示登录失败
	 */
	public int login(String code,String ip,String username,String password,int port);
	
	/**
	 * 设备操作
	 * @param code 设备编码
	 * @param loginUserHandle 登录会话标识/句柄
	 * @param command 操作命令
	 * @param params 命令参数
	 * @return true：操作成功 ，false：操作失败
	 */
	public boolean control(String code,int loginUserHandle,String command, Kv params);
}
